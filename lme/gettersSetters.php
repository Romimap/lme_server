<?php
    function quote ($str) {
        return "'" . $str . "'";
    }

    function signUp($email, $username, $pass, $isChild, $pdo) {
        $prepared = $pdo->prepare('SELECT p.id, c.id FROM parent AS p, child AS c WHERE p.email = :emailA OR c.email = :emailB OR p.pseudo = :usernameA OR c.pseudo = :usernameB');
        $values = [':emailA' => $email, ':emailB' => $email, ':usernameA' => $username, ':usernameB' => $username];

        if ($prepared->execute($values)) {
          if ($row = $prepared->fetch()) {
            echo '{"return":false}';
            return;
          }
          if ($isChild) {
            $prepared = $pdo->prepare('INSERT INTO child (email, pseudo, password) VALUES (:email, :username, :password)');
            $values = [':email' => $email, ':username' => $username, ':password' => $pass];
            if ($prepared->execute($values)) {
              echo '{"return":true}';
              return;
            }
            echo '{"return":false}';
            return;
          } else {
            $prepared = $pdo->prepare('INSERT INTO parent (email, pseudo, password) VALUES (:email, :username, :password)');
            $values = [':email' => $email, ':username' => $username, ':password' => $pass];
            if ($prepared->execute($values)) {
              echo '{"return":true}';
              return;
            }
            echo '{"return":false}';
            return;
          }
        }
    }

    function getChildList ($pdo) {
        $prepared = $pdo->prepare('SELECT id, pseudo FROM child WHERE parent_id = :id');
        $values = [':id' => $_SESSION['id']];
        $ids = [];
        if ($prepared->execute($values)) {
            $i = 0;
            while ($row = $prepared->fetch()) {
                $ids[$i] = $row;
                $i++;
            }
            echo '{"children" : ' . json_encode($ids) . "}";
        }
    }

    function getContactList ($child_id, $pdo) {
        $prepared = $pdo->prepare('SELECT id, nom, numero FROM contact WHERE id_child = :id_child');
        $values = [':id_child' => $child_id];
        $ids = [];
        if ($prepared->execute($values)) {
            $i = 0;
            while ($row = $prepared->fetch()) {
                $ids[$i] = $row;
                $i++;
            }
            echo '{"contacts" : ' . json_encode($ids) . "}";
        }
    }

    function getMediaList ($child_id, $pdo) {
        $prepared = $pdo->prepare('SELECT name, link, date FROM media WHERE id_child = :id_child');
        $values = [':id_child' => $child_id];
        if ($prepared->execute($values)) {
            echo(json_encode($prepared->fetchAll()));
        }
    }

    function getSMSList ($child_id, $contact_id, $pdo) {
        $prepared = $pdo->prepare('SELECT id, date_time, text_value, type_value FROM sms WHERE id_child = :id_child AND id_contact = :id_contact ORDER BY date_time DESC');
        $values = [':id_child' => $child_id, ':id_contact' => $contact_id];
        if ($prepared->execute($values)) {
            echo '{"SMS" : ' . (json_encode($prepared->fetchAll())) . '}';
        }
    }

    function getCallList ($child_id, $contact_id, $pdo) {
        $prepared = $pdo->prepare('SELECT id, date_time, type_value, duration FROM calls WHERE id_child = :id_child AND id_contact = :id_contact ORDER BY date_time DESC');
        $values = [':id_child' => $child_id, ':id_contact' => $contact_id];
        if ($prepared->execute($values)) {
            echo '{"Calls" : ' . (json_encode($prepared->fetchAll())) . '}';
        }
    }

    function addCalls ($calls, $id_child, $pdo) {
      foreach ($calls as $call) {
          $prepared = $pdo->prepare('SELECT * FROM contact WHERE numero = :contact_num AND id_child = :id_child');
          $values = [':contact_num' => $call['contact_num'], ':id_child' => $id_child];
          $prepared->execute($values);
          $id_contact = -1;
          if ($row = $prepared->fetch()) {
              $id_contact = $row['id'];
          } else {
              //Adding a new contact
              $prepared = $pdo->prepare('INSERT INTO contact (nom, numero, id_child) VALUES (:contact_name, :contact_num, :id_child)');
              $values = [':contact_name' => $call['contact_name'], ':contact_num' => $call['contact_num'], ':id_child' => $id_child];
              $prepared->execute($values);

              $prepared = $pdo->prepare('SELECT * FROM contact WHERE numero = :contact_num  AND id_child = :id_child');
              $values = [':contact_num' => $SMS['contact_num'], ':id_child' => $id_child];
              $prepared->execute($values);
              $row = $prepared->fetch();
              $id_contact = $row['id'];
          }

          $prepared = $pdo->prepare('INSERT INTO calls (date_time, type_value, id_child, id_contact) VALUES (:date_time, :type_value, :id_child, :id_contact)');
          $values = [ ':date_time' => $call['date_time'],
                      ':type_value' => $call['type_value'],
                      ':id_child' => $id_child,
                      ':id_contact' => $id_contact];
          $prepared->execute($values);
      }
      echo '{"addcall" : "addcall"}';
    }

    function addSMSs ($SMSs, $id_child, $pdo) {
        foreach ($SMSs as $SMS) {
            $prepared = $pdo->prepare('SELECT * FROM contact WHERE numero = :contact_num AND id_child = :id_child');
            $values = [':contact_num' => $SMS['contact_num'], ':id_child' => $id_child];
            $prepared->execute($values);
            $id_contact = -1;
            if ($row = $prepared->fetch()) {
                $id_contact = $row['id'];
            } else {
                //Adding a new contact
                $prepared = $pdo->prepare('INSERT INTO contact (nom, numero, id_child) VALUES (:contact_name, :contact_num, :id_child)');
                $values = [':contact_name' => $SMS['contact_name'], ':contact_num' => $SMS['contact_num'], ':id_child' => $id_child];
                $prepared->execute($values);

                $prepared = $pdo->prepare('SELECT * FROM contact WHERE numero = :contact_num  AND id_child = :id_child');
                $values = [':contact_num' => $SMS['contact_num'], ':id_child' => $id_child];
                $prepared->execute($values);
                $row = $prepared->fetch();
                $id_contact = $row['id'];
            }

            $prepared = $pdo->prepare('INSERT INTO sms (date_time, text_value, type_value, id_child, id_contact) VALUES (:date_time, :text_value, :type_value, :id_child, :id_contact)');
            $values = [ ':date_time' => $SMS['date_time'],
                        ':text_value' => $SMS['text_value'],
                        ':type_value' => $SMS['type_value'],
                        ':id_child' => $id_child,
                        ':id_contact' => $id_contact];
            $prepared->execute($values);
        }
        echo '{"addSMS" : "addSMS"}';
    }

    function getGPS ($id_child, $pdo) {
        $prepared = $pdo->prepare('SELECT lon, lat FROM child WHERE id = :id_child');
        $values = [':id_child' => $id_child];
        if ($prepared->execute($values)) {
            if ($row = $prepared->fetch()) {
              echo '{"return":true, "lon":'.$row['lon'].', "lat":'.$row['lat'].'}';
              return;
            }
        }
        echo '{"return":false;}';
    }

    function setGPS ($id_child, $lon, $lat, $pdo) {
        $prepared = $pdo->prepare('UPDATE child SET lon = :lon, lat = :lat WHERE id = :id_child');
        $values = [ ':lat' => $lat,
                    ':lon' => $lon,
                    ':id_child' => $id_child];
        $prepared->execute($values);
    }

    function getLastSMSDate ($id_child, $pdo) {
        $prepared = $pdo->prepare('SELECT date_time FROM sms WHERE id_child = :id_child ORDER BY date_time DESC LIMIT 1');
        $values = [ ':id_child' => $id_child];
        if ($prepared->execute($values)) {
            if ($row = $prepared->fetch()) {
                echo '{"date": "' . $row['date_time'] . '"}';
                return;
            }
        }

        echo '{"date": "1970-01-01 00:00:00"}';
    }

    function getParent ($id_child, $pdo) {
        $prepared = $pdo->prepare('SELECT p.id, p.email, p.pseudo FROM parent AS p, child AS c WHERE c.parent_id = p.id AND c.id = :id_child');
        $values = [ ':id_child' => $id_child];
        if ($prepared->execute($values)) {
            if ($row = $prepared->fetch()) {
                echo '{"parent": true, "name": ' . $row['pseudo'] .', "email": ' . $row['email'] .', "id": ' . $row['id'] .'}';
                return;
            }
        }
        echo '{"parent": false}';
    }

    function setParent ($id_child, $user, $pass, $pdo) {
        $prepared = $pdo->prepare("SELECT id FROM parent WHERE email = :user AND password = :pass");
        $values = array(":user" => $user, ":pass" => $pass);
        if ($prepared->execute($values)) {
            if ($row = $prepared->fetch()) {
              $prepared = $pdo->prepare('UPDATE child SET parent_id = :parent_id WHERE id = :id_child');
              $values = [':parent_id' => $row['id'],
                          ':id_child' => $id_child];
              $prepared->execute($values);
            }
        }
    }

    function rmChild ($id_child, $pdo) {

    }

    function addImages ($id_child, $images, $dates, $pdo) {
        echo "images : ";
        $date = 0;
        foreach ($images as $key => $image) {
            $target_dir = "images/";
            $target_file = $target_dir . basename($image["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($image["tmp_name"]);
                if($check == false) {
                    echo '{"return":false, "message":"not an image"}';
                    return;
                }
            }

            // Check if file already exists
            if (file_exists($target_file)) {
                $i = 0;
                do {
                    $target_file = $target_dir . $i . basename($image["name"]);
                    $i++;
                } while (file_exists($target_file));
            }

            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo '{"return":false, "message":"format not allowed"}';
                return;
            }

            if (move_uploaded_file($image["tmp_name"], $target_file)) {
                $prepared = $pdo->prepare("INSERT INTO media (name, link, date_time, type, id_child) VALUES (:name, :link, :date_time, :type, :id_child)");
                $values = [":name" => basename($target_file), ":link" => $target_file, ":date_time" => $dates[$date], ":type" => "image", ":id_child" => $id_child];
                if ($prepared->execute($values)) {
                    echo '{"return":true, "message":"file '. $target_file .' uploaded"}';
                } else {
                    echo '{"return":false, "message":"SQL error"}';
                    return;
                }
            } else {
                echo '{"return":false, "message":"error uploading the file"}';
                return;
            }
            $date++;
        }
    }

    function getImages ($id_child, $pdo) {
        $prepared = $pdo->prepare('SELECT id, name, link, date_time FROM media WHERE id_child = :id_child AND type = "image"');
        $values = [ ':id_child' => $id_child];
        if ($prepared->execute($values)) {
            echo '{"images": ' . json_encode($prepared->fetchAll()) . '}';
            return;
        }
        echo '{"parent": false}';
    }

    function addVideos ($id_child, $videos, $dates, $pdo) {
        echo "videos : ";
        $date = 0;
        foreach ($videos as $key => $video) {
            $target_dir = "videos/";
            $target_file = $target_dir . basename($video["name"]);
            $uploadOk = 1;
            $videoFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // Check if file already exists
            if (file_exists($target_file)) {
                $i = 0;
                do {
                    $target_file = $target_dir . $i . basename($video["name"]);
                    $i++;
                } while (file_exists($target_file));
            }

            // Allow certain file formats
            if($videoFileType != "mp4" && $videoFileType != "3gp" && $videoFileType != "webm"
            && $videoFileType != "ts" && $videoFileType != "mkv" ) {
                echo '{"return":false, "message":"format not allowed"}';
                return;
            }

            if (move_uploaded_file($video["tmp_name"], $target_file)) {
                $prepared = $pdo->prepare("INSERT INTO media (name, link, date_time, type, id_child) VALUES (:name, :link, :date_time, :type, :id_child)");
                $values = [":name" => basename($target_file), ":link" => $target_file, ":date_time" => $dates[$date], ":type" => "video", ":id_child" => $id_child];
                if ($prepared->execute($values)) {
                    echo '{"return":true, "message":"file '. $target_file .' uploaded"}';
                } else {
                    echo '{"return":false, "message":"SQL error"}';
                    return;
                }
            } else {
                echo '{"return":false, "message":"error uploading the file"}';
                return;
            }
            $date++;
        }
    }

    function getVideos ($id_child, $pdo) {
        $prepared = $pdo->prepare('SELECT id, name, link, date_time FROM media WHERE id_child = :id_child AND type = "video"');
        $values = [ ':id_child' => $id_child];
        if ($prepared->execute($values)) {
            echo '{"videos": ' . json_encode($prepared->fetchAll()) . '}';
            return;
        }
        echo '{"parent": false}';
    }
?>
