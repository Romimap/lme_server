<?php
    ini_set('display_errors',1);
    $JSON = json_decode($_POST['json'], true);

    if (!isset($JSON['type'])) {
        exit();
    }
    if (isset($JSON['sid'])) {
        session_id($JSON['sid']);
    }
    session_start();

    require('sql.php');
    require('gettersSetters.php');

    switch ($JSON['type']) {
        case 'Connect':
            if (!isset($JSON['login'])
            || !isset($JSON['passwd'])) {
                echo '{"return": false}';
                exit();

            }
            $login = $JSON['login']; //TODO: escape string
            $passwd = $JSON['passwd']; //TODO: escape string

            $prepared = $pdo->prepare("SELECT id, email FROM parent WHERE email = :login AND password = :passwd");
            $values = array(":login" => $login, ":passwd" => $passwd);
            if ($prepared->execute($values)) {
                if ($row = $prepared->fetch()) {
                    $_SESSION['login'] = $login;
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['role'] = 'parent';
                    echo '{"return": true, "sid": ' . session_id() . ', "role":' . $_SESSION['role'] .'}';
                    exit();
                } else {
                    $prepared = $pdo->prepare("SELECT id, email FROM child WHERE email = :login AND password = :passwd");
                    if ($prepared->execute($values)) {
                        if ($row = $prepared->fetch()) {
                            $_SESSION['login'] = $login;
                            $_SESSION['id'] = $row['id'];
                            $_SESSION['role'] = 'child';
                            echo '{"return": true, "sid": ' . session_id() . ', "role":' . $_SESSION['role'] .'}';
                            exit();
                        } else {
                            echo '{"return": false, "message": "user or pass not valid"}';
                            exit();
                        }
                    }
                }
            } else {
              echo '{"return": false, "message": "query error"}';
              exit();
            }
            exit();

        case 'SignUp':
          signUp($JSON['email'], $JSON['username'], $JSON['pass'], $JSON['isChild'], $pdo);
          exit();

        case 'SMS':
            getSMSList($JSON['ChildId'], $JSON['ContactId'], $pdo);
            exit();

        case 'ChildList':
            getChildList($pdo);
            exit();

        case 'ContactList':
            getContactList($JSON['ChildId'], $pdo);
            exit();

        case 'Calls':
            getCallList($JSON['ChildId'], $JSON['ContactId'], $pdo);
            exit();

        case 'AddCalls':
            addCalls($JSON['calls'], $_SESSION['id'], $pdo);
            exit();

        case 'AddSMS':
            addSMSs($JSON['SMS'], $_SESSION['id'], $pdo);
            exit();

        case 'GetGPS':
            getGPS($JSON['ChildId'], $pdo);
            exit();

        case 'SetGPS':
            setGPS($_SESSION['id'], $JSON['lon'], $JSON['lat'], $pdo);
            exit();

        case 'SMSLastDate':
            getLastSMSDate($_SESSION['id'], $pdo);
            exit();

        case 'GetParent':
            getParent($_SESSION['id'], $pdo);
            exit();

        case 'SetParent':
            setParent($_SESSION['id'], $JSON['user'], $JSON['pass'], $pdo);
            exit();

        case 'SendImages':
            addImages($_SESSION['id'], $_FILES, $JSON['dates'], $pdo);
            exit();

        case 'GetImages':
            getImages($JSON['IdChild'], $pdo);
            exit();

        case 'SendVideos':
            addVideos($_SESSION['id'], $_FILES, $JSON['dates'], $pdo);
            exit();

        case 'GetVideos':
            getVideos($JSON['IdChild'], $pdo);
            exit();

        default:
            echo '{"oopsie"="oops"}';
            break;
    }


?>
