<?php
    if (!isset($_POST['type'])) {
        exit();
    }
    if (isset($_POST['sid'])) {
        session_id($_POST['sid']);
    }
    session_start();

    require('sql.php');
    require('gettersSetters.php');

    switch ($_POST['type']) {
        case 'Connect':
            if (!isset($_POST['login'])
            || !isset($_POST['passwd'])
            || !isset($_POST['role'])) {
                echo '{"return": false}';
                exit();

            }
            $login = $_POST['login']; //TODO: escape string
            $passwd = $_POST['passwd']; //TODO: escape string
            $prepared;

            if ($_POST['role'] == "parent") {
                $prepared = $pdo->prepare("SELECT id, email FROM parent WHERE email = :login AND password = :passwd");
            } else if ($_POST['role'] == "child") {
                $prepared = $pdo->prepare("SELECT id, email FROM child WHERE email = :login AND password = :passwd");
            } else {
                echo '{"return": false}';
                exit();

            }

            $values = array(":login" => $login, ":passwd" => $passwd);
            if ($prepared->execute($values) && $row = $prepared->fetch()) {
                $_SESSION['login'] = $login;
                $_SESSION['id'] = $row['id'];
                $_SESSION['role'] = $_POST['role'];
                echo '{"return": true, "sid": ' . session_id() . '}';
                exit();

            } else {
                echo '{"return": false}';
                exit();

            }
            break;

        case 'SMS':
            getSMSList(1, 1, $pdo);
            break;

        default:
            echo '[]';
            break;
    }


?>
